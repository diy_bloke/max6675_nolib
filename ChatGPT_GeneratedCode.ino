#include <ESP8266WiFi.h>

// Pin connections
const int CLK_PIN = D5;   // CLK pin (SCLK)
const int CS_PIN = D6;    // CS pin (CS)
const int DO_PIN = D7;    // DO pin (MISO)

// Function to read the temperature from MAX6675
double readTemperature() {
  // Initialize the variables
  int data = 0;
  double temperature = 0.0;

  // Start the conversion
  digitalWrite(CS_PIN, LOW);
  delayMicroseconds(100); // Wait for a short delay

  // Read the data
  for (int i = 15; i >= 0; i--) {
    digitalWrite(CLK_PIN, HIGH);
    delayMicroseconds(100); // Wait for a short delay
    data += digitalRead(DO_PIN) << i;
    digitalWrite(CLK_PIN, LOW);
    delayMicroseconds(100); // Wait for a short delay
  }

  // Stop the conversion
  digitalWrite(CS_PIN, HIGH);

  // Check for error
  if (data & 0x4) {
    Serial.println("Error: Thermocouple is not connected properly.");
    return 0.0;
  }

  // Get the temperature in Celsius
  temperature = (data >> 3) * 0.25;

  return temperature;
}

void setup() {
  // Initialize the serial communication
  Serial.begin(9600);

  // Set the pin modes
  pinMode(CLK_PIN, OUTPUT);
  pinMode(CS_PIN, OUTPUT);
  pinMode(DO_PIN, INPUT);
}

void loop() {
  // Read the temperature
  double temperature = readTemperature();

  // Print the temperature
  Serial.print("Temperature: ");
  Serial.print(temperature);
  Serial.println(" °C");

  delay(1000);
}
