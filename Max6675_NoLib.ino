//PinNrs for ESP8266
#define cs D8 //cs
#define clk D5 //sck
#define miso D6 // so
//define variables
int v = 0;
float tempC;

void setup() {
Serial.begin(115200);
pinMode(cs, OUTPUT);
pinMode(clk, OUTPUT);
pinMode(miso, INPUT);

digitalWrite(cs, HIGH);
digitalWrite(clk, LOW);
}

void loop() {
v = spiRead();
if (v == -1) {
Serial.print("niet gevonden");
}
else {
tempC = v * 0.25;
Serial.println(tempC);
}
delay(500);
}

int spiRead() {
int rawTmp = 0;
digitalWrite(cs,LOW);
delay(2);
digitalWrite(cs,HIGH);
delay(200);

//Pull CS low start conversion

digitalWrite(cs,LOW);
// Read MSB (bit15) and discard it 
digitalWrite(clk,HIGH);
delay(1);
digitalWrite(clk,LOW);

//Read bits 14-0 from MAX6675, store in tempC
// it is also possible to only read the 12 bits you need and omit the rightshift 3 at the end
for (int i=14; i>=0; i--) {
digitalWrite(clk,HIGH);
rawTmp += digitalRead(miso) << i;
digitalWrite(clk,LOW);
}
//if bit D2 HIGH no sensor
if ((rawTmp & 0x04) == 0x04) return -1;

// shift right three places to get rid of b0,b1 and b2
return rawTmp >> 3;
}